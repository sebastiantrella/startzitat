-- phpMyAdmin SQL Dump
-- version 2.9.1.1-Debian-10
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Erstellungszeit: 16. Mai 2009 um 17:46
-- Server Version: 5.0.32
-- PHP-Version: 5.2.0-8+etch13
-- 
-- Datenbank: `startzitatsql1`
-- 
CREATE DATABASE `startzitatsql1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `startzitatsql1`;

-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `zitate`
-- 

CREATE TABLE `zitate` (
  `id` int(11) NOT NULL auto_increment,
  `zitat` text character set utf8 NOT NULL,
  `aktiv` binary(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- 
-- Daten für Tabelle `zitate`
-- 

